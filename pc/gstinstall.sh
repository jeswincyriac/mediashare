which  pacman  >/dev/null 2>&1
if [ $? -eq 0 ]
then
for package in gstreamer gst-plugins-base gst-plugins-good gst-plugins-bad

do
if pacman -Qs $package  > /dev/null ; then
  echo "The package $package is installed"
else
  echo "The package $package is not installed" && sudo pacman -S $package --noconfirm
fi
done
fi



which  apt >/dev/null 2>&1
if [ $? -eq 0 ]
then
for package in gstreamer gst-plugins-base gst-plugins-good gst-plugins-bad

do
if dpkg -s $package  > /dev/null ; then
  echo "The package $package is installed"
else
  echo "The package $package is not installed" && sudo apt install $package -y
fi
done
fi
