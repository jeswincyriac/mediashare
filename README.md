# MediaShare
## Introduction
A  typical  android  smartphone  has  a  lot  of  peripherals  packed  into  a  small  form  fac-tor.  It has fingerprint scanner,speakers,microphone,camera etc.This project identifies amechanism to efficiently interface streaming pipelines to the native device interface of aPersonal Computer running Linux based operating system.  Unlike currently availablestreaming solutions which are specific in nature,  this technique will provide a generalinterface.  Our project is also one of the first in the community which attempts to achievePAM unlock of PC remotely using smartphone biometrics.

## Functions
4 functions are planned to be in the version 1.0 release
1. Speaker share (PC to phone)
2. Mic share (Phone to PC)
3. Webcam share (Phone to PC)
4. Biometric (Phone to PC)
