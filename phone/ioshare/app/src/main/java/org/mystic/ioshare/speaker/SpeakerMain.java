package org.mystic.ioshare.speaker;
// This package contains all Speaker functionality related Classes and methods.

import android.util.Log;

import org.freedesktop.gstreamer.GstUtilities;

public class SpeakerMain extends GstUtilities{
    public SpeakerMain(String port) {
        // construct pipeline
        Log.d("io-share","port is "+port);
        nativePipelineString = "udpsrc caps=\"application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00\" port="+port+
                " ! rtpjitterbuffer latency=200 ! rtpopusdepay ! opusdec plc=true ! openslessink";
        Log.d("io-share","Pipeline is "+nativePipelineString);
        // TODO : optionally get custom pipeline from advanced settings
    }

}
