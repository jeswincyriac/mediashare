package org.mystic.ioshare.mic;
// This package contains all Mic functionality related Classes and methods.

import org.freedesktop.gstreamer.GstUtilities;

public class MicMain extends GstUtilities{
    public MicMain(String IP, String port) {
        // construct pipeline
        nativePipelineString = "openslessrc ! audioconvert  ! audioresample ! opusenc ! rtpopuspay ! udpsink host="+IP+" port="+port;
        // TODO : Optionally get custom pipeline from advanced settings
    }

}
