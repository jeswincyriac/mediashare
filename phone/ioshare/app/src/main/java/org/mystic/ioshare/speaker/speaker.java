package org.mystic.ioshare.speaker;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.mystic.ioshare.MainActivity;
import org.mystic.ioshare.R;

import androidx.annotation.RequiresApi;

public class speaker extends Service {
    // TODO: BUG <=> If a GStreamer error occurs then this service shouldn't start.
    // TODO: BUG <=> Starting and stopping service multiple times casues it to not work.

    // TODO: More than 99% of speaker and mic service are the same, so make them a single multi-threaded service
    private static final int NOTIFICATION_ID = 1;
    public static boolean speakerServiceRunning = false;
    private SpeakerMain mainSpeaker = null;

    public speaker() {
    }

    @Override
    public void onCreate(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String port = prefs.getString("speaker_port","");
        Log.d("io-share","port is "+port);
        mainSpeaker = new SpeakerMain(port);
        mainSpeaker.init(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        // TODO: Put a pause/stop button action on the notification
        Notification notification =
                new Notification.Builder(this, "ioshare-notify")
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_speaker_message))
                        .setSmallIcon(R.drawable.baseline_settings_white_18dp)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(NOTIFICATION_ID, notification);
        if(mainSpeaker.isPlaying()) mainSpeaker.pause();
        speakerServiceRunning = true;
        try {
            // wait till the native code initializes the pipeline
            Thread.sleep(10); 
            // TODO : Make pipeline variable thread-safe instead of hardcoding wait time

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainSpeaker.play();
        Toast.makeText(this, "Speaker service starting", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy(){
        mainSpeaker.finalize();
        speakerServiceRunning = false;
        Toast.makeText(this, "Speaker service stopping", Toast.LENGTH_SHORT).show();
    }
}
