package org.mystic.ioshare.mic;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import org.mystic.ioshare.MainActivity;
import org.mystic.ioshare.R;

import androidx.annotation.RequiresApi;

public class mic extends Service {

    private static final int NOTIFICATION_ID = 2;
    public static boolean micServiceRunning = false;
    private MicMain mainMic = null;

    public mic() {
    }

    @Override
    public void onCreate(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String ipaddress = prefs.getString("pc_ip","");
        String port = prefs.getString("mic_port","");
        mainMic = new MicMain(ipaddress,port);
        mainMic.init(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        // TODO: Put a pause/stop button action on the notification
        Notification notification =
                new Notification.Builder(this, "ioshare-notify")
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_mic_message))
                        .setSmallIcon(R.drawable.baseline_settings_white_18dp)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(NOTIFICATION_ID, notification);
        if(mainMic.isPlaying()) mainMic.pause();
        micServiceRunning = true;
        try {
            // wait till the native code initializes the pipeline
            Thread.sleep(10);
            // TODO : Make pipeline variable thread-safe instead of hardcoding wait time
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainMic.play();
        Toast.makeText(this, "Mic service starting", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy(){
        mainMic.finalize();
        micServiceRunning = false;
        Toast.makeText(this, "Mic service stopping", Toast.LENGTH_SHORT).show();
    }
}
