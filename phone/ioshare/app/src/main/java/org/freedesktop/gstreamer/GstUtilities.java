package org.freedesktop.gstreamer;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.mystic.ioshare.MainActivity;
import org.mystic.ioshare.R;

public class GstUtilities {
    protected native void nativeInit();
    protected static native boolean nativeClassInit();
    protected native void nativeFinalize();
    protected native void nativePlay();
    protected native void nativePause();
    protected String nativePipelineString;
    protected long native_custom_data;
    protected static boolean Initialized = false;
    protected boolean is_playing = false;

    public int init(Context serviceContext){
       //Initialize GStreamer and warn if it fails
        try {
            GStreamer.init(serviceContext);
        } catch (Exception e) {
            return 1;
        }
        nativeClassInit();
        nativeInit();
        Initialized = true;
        return 0;
    }

    public void finalize(){
        Initialized = false;
        nativeFinalize();
    }

    public static boolean isInitialized(){
        return Initialized;
    }

    public boolean play(){
        nativePlay();
        is_playing = true;
        return true;
    }

    public boolean pause(){
        nativePause();
        is_playing = false;
        return true;
    }
    public boolean isPlaying(){
        return is_playing;
    }

    protected void onGStreamerInitialized(){

    }

    protected void setMessage(String message){
        //final TextView tv = (TextView) this.findViewById(R.id.textview_message);
        //runOnUiThread (new Runnable() {public void run() {tv.setText(message);}});
        // TODO: Put messages from GStreamer on the screen of MainActivity is Show Error Messages is turned on
    }

    static {
        System.loadLibrary("gstreamer_android");
        System.loadLibrary("gst-utils");
    }

}
